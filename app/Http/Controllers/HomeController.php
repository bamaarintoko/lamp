<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 5/5/2019
 * Time: 3:03 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
}